package sn.kader.accountapi.account.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sn.kader.accountapi.account.dto.AccountResponse;
import sn.kader.accountapi.account.service.AccountService;

@RestController
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/accounts")
    ResponseEntity<AccountResponse> getAccounts(){
        return accountService.getAccounts();
    }
}
