package sn.kader.accountapi.account.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class AccountResponse {
    private List<String> accounts;
    private String port;
}
