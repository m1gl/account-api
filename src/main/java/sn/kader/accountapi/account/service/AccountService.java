package sn.kader.accountapi.account.service;

import org.springframework.http.ResponseEntity;
import sn.kader.accountapi.account.dto.AccountResponse;

public interface AccountService {
    ResponseEntity<AccountResponse> getAccounts();
}
