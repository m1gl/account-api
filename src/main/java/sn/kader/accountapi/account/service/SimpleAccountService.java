package sn.kader.accountapi.account.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import sn.kader.accountapi.account.dto.AccountResponse;

import java.util.Arrays;

@Service
public class SimpleAccountService implements AccountService {
    @Value("${server.port}")
    private String port;
    @Override
    public ResponseEntity<AccountResponse> getAccounts() {
        return ResponseEntity.ok(AccountResponse
                .builder()
                .accounts(Arrays.asList("abdou", "moussa"))
                .port(port)
                .build());
    }
}
